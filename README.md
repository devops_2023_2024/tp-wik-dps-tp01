# Simple Node.js API

Ce projet consiste en une API Node.js simple qui retourne les headers de la requête au format JSON lorsqu'une requête GET est effectuée sur "/ping".

## Installation

1. Clonez le dépôt :

   ```bash
   git clone https://github.com/votre-utilisateur/simple-nodejs-api.git

2. Accédez au répertoire du projet :

    ```bash
    cd simple-nodejs-api

3. Installez les dépendances :

    ```bash
    npm install

## Utilisation

1. Démarrez le serveur :

    ```bash
    node app.js
    ```
Le serveur sera accessible à l'adresse http://localhost:3000.

2. Effectuez une requête GET sur "/ping" pour obtenir les headers de la requête au format JSON.

## Exemple de Requête
    curl http://localhost:3000/ping

![](./img/its-up-to-you-frank-sinatra.gif)
